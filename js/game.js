const FIXED_PHASE = 0;

const scoreString = 'Score : ';

let bulletTime = 0;
let score = 0;
let firingTimer = 0;
let bigAlienFiringTimer = 0;
let cdTimer = 0;
let liveNum = 100;
let skillNum = 0;
let ultimateNum = 0;
let ultimateNumCdTime = 0;
let skillEndTime = 0;
let isMakingEnemy = false;

let storedData = null;
let isProcess = false;

let totalVolume = 1;

let phase = FIXED_PHASE;

function getRandom(min, max){
    return Math.floor(Math.random() * max) + min;
};

var mainState = {
    preload: function() {
        game.load.image('background', 'assets/background.png');
        game.load.image('airplane', 'assets/airplane.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('livebar', 'assets/livebar.png');
        game.load.image('bigAlien', 'assets/big_alien.png');
        game.load.image('bigAlienLivebar', 'assets/big_alien_livebar.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('game_over', 'assets/game_over.png');
        game.load.image('hospital', 'assets/hospital.png');
        game.load.image('speakerLoud', 'assets/speaker_loud.png');
        game.load.image('speakerSmall', 'assets/speaker_small.png');
        game.load.image('volumnBar', 'assets/volumnBar.png');

        game.load.audio('shooting', 'assets/shooting.mp3');
        game.load.audio('bgm', 'assets/bgm.mp3');

        game.load.spritesheet('caveman', 'assets/caveman.png', 32, 32);
        game.load.spritesheet('invader', 'assets/32x32-bat-sprite.png', 32, 32);
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    },
    create: function() {
        // Initialize Game Physics Engine
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.stage.backgroundColor = '#33313b';

        // Mouse / Keyboard Setting
        this.cursor = game.input.keyboard.createCursorKeys();
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.skillButton = game.input.keyboard.addKey(Phaser.Keyboard.E);
        this.enterButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.ultimateSkillButton = game.input.keyboard.addKey(Phaser.Keyboard.W);

        // Background Setting
        this.backgroundOne = game.add.sprite(0, 0, 'background'); 
        game.physics.arcade.enable(this.backgroundOne);
        this.backgroundOne.body.velocity.y = 200;

        this.backgroundTwo = game.add.sprite(0, -1563, 'background'); 
        game.physics.arcade.enable(this.backgroundTwo);
        this.backgroundTwo.body.velocity.y = 200;

        // Airplane Setting
        this.airplane = game.add.sprite(DISPLAY_WIDTH/2, DISPLAY_HEIGHT - 200, 'airplane');
        game.physics.arcade.enable(this.airplane);
        this.airplane.anchor.setTo(0.5, 0.5);

        // Bullet Setting
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

        this.hospitals = game.add.group();
        this.hospitals.enableBody = true;
        this.hospitals.physicsBodyType = Phaser.Physics.ARCADE;
        this.hospitals.createMultiple(30, 'hospital');
        this.hospitals.setAll('anchor.x', 0.5);
        this.hospitals.setAll('anchor.y', 1);
        this.hospitals.setAll('outOfBoundsKill', true);
        this.hospitals.setAll('checkWorldBounds', true);

        // Enemy Seeting
        this.aliens = game.add.group();
        this.aliens.enableBody = true;
        this.aliens.physicsBodyType = Phaser.Physics.ARCADE;

        this.aliensFour = game.add.group();
        this.aliensFour.enableBody = true;
        this.aliensFour.physicsBodyType = Phaser.Physics.ARCADE;

        this.cavemans = game.add.group();
        this.cavemans.enableBody = true;
        this.cavemans.physicsBodyType = Phaser.Physics.ARCADE;

        // Big Alien
        this.bigAlienLivebar = game.add.sprite(DISPLAY_WIDTH/2 - 75, 70, 'bigAlienLivebar');
        this.bigAlienLivebar.visible = false;

        this.gameOver = game.add.sprite(DISPLAY_WIDTH/2, DISPLAY_HEIGHT/2, 'game_over');
        game.physics.arcade.enable(this.gameOver);
        this.gameOver.anchor.setTo(0.5, 0.5);
        this.gameOver.visible = false;

        // The enemy's bullets
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        // An explosion pool
        this.explosions = game.add.group();
        this.explosions.createMultiple(300, 'kaboom');
        this.explosions.forEach(this.setupInvader, this);

        // The Emmiter
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        // Create Right Side Score Board
        this.scoreText = game.add.text(800, 100, scoreString + score, { font: '34px Arial', fill: '#fdcb6e' });

        this.live = game.add.text(800, 200, 'Live :', { font: '34px Arial', fill: '#fdcb6e' });
        this.livebar = game.add.sprite(800, 250, 'livebar');

        this.ultimateSkill = game.add.text(800, 350, 'Ultimate Skill :', { font: '34px Arial', fill: '#fdcb6e' });
        this.ultimateSkillbar = game.add.sprite(800, 400, 'livebar');

        this.invisibleSkill = game.add.text(800, 500, 'Invisible CD :', { font: '34px Arial', fill: '#fdcb6e' });
        this.invisibleSkillBar = game.add.sprite(800, 550, 'livebar');

        this.speakerLoud = game.add.sprite(800, 650, 'speakerLoud');
        this.speakerSmall = game.add.sprite(900, 650, 'speakerSmall');

        // Audio Sound
        this.shootingSound = game.add.audio('shooting');
        this.bgmSound = game.add.audio('bgm');
        this.bgmSound.play('', 0, totalVolume, true, false);

        // Click Audio
        this.speakerLoud.inputEnabled = true;
        this.speakerSmall.inputEnabled = true;
        this.speakerSmall.events.onInputDown.add(() => {
            if (totalVolume - 0.1 >= 0) totalVolume -= 0.1;
            else totalVolume = 0;
        }, this);

        this.speakerLoud.events.onInputDown.add(() => {
            if (totalVolume + 0.1 <= 1) totalVolume += 0.1;
            else totalVolume = 1;
        }, this);

        // Volumn Bar
        this.volumnBar = game.add.sprite(800, 700, 'volumnBar');

        this.alphaAirplane = game.add.tween(this.airplane).to({ alpha: 0.5 }, 100, Phaser.Easing.Linear.None).yoyo(true);

        window['phaser'] = this;
    },
    update: function() {

        game.sound.volume = totalVolume;
        this.moveAirplane();
        this.backgroundCombine();

        this.scoreText.text = scoreString + score;

        if (game.time.now > firingTimer) this.enemyFires();
        if (game.time.now > cdTimer) {
            if (skillNum <= 99) skillNum += 1;
            if (ultimateNum <= 99 && !this.ultimateSkillButton.isDown && game.time.now > ultimateNumCdTime) {
                ultimateNum += 1;
            }
            cdTimer += 100;
        }

        game.physics.arcade.overlap(this.hospitals, this.airplane, this.gainLive, null, this);
        game.physics.arcade.overlap(this.cavemans, this.bullets, this.cavemanHitBullet, null, this);

        this.livebar.scale.setTo(liveNum/100, 1.0);
        this.ultimateSkillbar.scale.setTo(ultimateNum/100, 1.0);
        this.invisibleSkillBar.scale.setTo(skillNum/100, 1.0);
        this.volumnBar.scale.setTo(totalVolume/1, 1.0);

        game.physics.arcade.overlap(this.bullets, this.aliens, this.collisionHandler, null, this);
        game.physics.arcade.overlap(this.bullets, this.aliensFour, this.collisionHandlerFour, null, this);
        if (skillEndTime >= game.time.now) {
            this.alphaAirplane.start();
        }
        else {
            game.physics.arcade.overlap(this.enemyBullets, this.airplane, this.enemyBulletHitsPlayer, null, this);
            game.physics.arcade.overlap(this.aliens, this.airplane, this.enemyHitsPlayer, null, this);
            game.physics.arcade.overlap(this.aliensFour, this.airplane, this.enemyHitsPlayer, null, this);
            game.physics.arcade.overlap(this.cavemans, this.airplane, this.cavemanHitAirplane, null, this);
            game.physics.arcade.overlap(this.bigAlien, this.airplane, this.bigAlienHitAirplane, null, this);
        }

        if (phase === 5 && this.bigAlien) {
            this.bigAlienLivebar.visible = true;

            game.physics.arcade.overlap(this.bullets, this.bigAlien, this.bulletHitsBigAlien, null, this);
        }

        if (liveNum <= 0) {
            this.gameOverState();
        }

        if (this.bigAlien && this.bigAlien.liveNum <= 0) {
            this.gameOverState();
        }

        // RegenerateEnemies
        const enemyAllDie = this.aliens.countLiving() === 0;
        const cavemanAllDie = this.cavemans.countLiving() === 0;
        if (((enemyAllDie && phase < 4) || (cavemanAllDie && phase === 4)) && !isMakingEnemy) {
            phase++;
            this.phaseText = game.add.text(DISPLAY_WIDTH/2, 100, `Phase ${phase}`, { font: '68px Arial', fill: '#fff' });
            this.phaseText.anchor.setTo(0.5, 0.5);
            const waitTime = 1000;
            game.add.tween(this.phaseText).to({ alpha: 0 }, waitTime, Phaser.Easing.Linear.None, true, 1000, 0, false);
            isMakingEnemy = true;
            setTimeout(() => { 
                this.createEnemies();
                isMakingEnemy = false;
            }, waitTime + 1000);
        }
    },
    setupInvader: function(invader) {
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    },
    bigAlienHitAirplane: function(airplane, bigAlien) {
        airplane.kill();

        const explosion = this.explosions.getFirstExists(false);
        if (explosion) {
            explosion.reset(airplane.x, airplane.y);
            explosion.play('kaboom', 30, false, true);
        }
    },
    moveAirplane: function () {
        if (this.cursor.left.isDown && this.airplane.body.x >= 0) {
            this.airplane.x -= 10;
        }
        else if (this.cursor.right.isDown && (this.airplane.body.x + this.airplane.width) <= DISPLAY_WIDTH) {
            this.airplane.x += 10;
        }
        else if (this.cursor.up.isDown && this.airplane.body.y >= 0) {
            this.airplane.y -= 10;
        }
        else if (this.cursor.down.isDown && (this.airplane.body.y + this.airplane.height) <= DISPLAY_HEIGHT) {
            this.airplane.y += 10;
        }

        if (this.fireButton.isDown) {
            this.fireBullet();
        }

        if (this.ultimateSkillButton.isDown && ultimateNum >= 0) {
            const perDecrease = .2;
            if (ultimateNum >= perDecrease) ultimateNum -= perDecrease;
            else ultimateNum = 0;
            ultimateNumCdTime = game.time.now + 3000;
            this.ultimateSkillProcess();
        }

        if (this.skillButton.isDown && skillNum >= 100) {
            skillNum = 0;
            skillEndTime = game.time.now + 3000;
        }
    },
    ultimateSkillProcess: function () {
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime && ultimateNum > 0 && this.airplane.visible) {
            //  Grab the first bullet we can from the pool
            // let bullet = this.bullets.getFirstExists(false);
            const centerX = this.airplane.x;
            const centerY = this.airplane.y - 15;
            let bulletSet = [];
            bulletSet[0] = game.add.sprite(centerX - 20, centerY, 'bullet', 0, this.bullets);
            bulletSet[1] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            bulletSet[2] = game.add.sprite(centerX + 20, centerY, 'bullet', 0, this.bullets);
            bulletSet[3] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            bulletSet[4] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            bulletSet[5] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            bulletSet[6] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            for (let i=0;i<7;i++) {
                bulletSet[i].body.velocity.y = -800;
            }
            bulletSet[3].body.velocity.x = -200;
            bulletSet[4].body.velocity.x = 200;
            bulletSet[5].body.velocity.x = -400;
            bulletSet[6].body.velocity.x = 400;

            bulletSet[3].angle = -10;
            bulletSet[5].angle = -30;
            bulletSet[4].angle = 10;
            bulletSet[6].angle = 30;
            bulletTime = game.time.now + 200;
            this.shootingSound.play();
        }
    },
    backgroundCombine: function() {
        if (this.backgroundOne.y >= 750) {
            this.backgroundOne.y -= 1563*2;
        }
        if (this.backgroundTwo.y >= 750) {
            this.backgroundTwo.y -= 1563*2;
        }
    },
    createEnemies: async function () {
        switch(phase) {
            case 1: {
                for (let y = 0; y < 10; y++) {
                    for (let x = 0; x < 10; x++) {
                        const alien = this.createEnemy(x * 48 + 150, y * 30 - 100);
                        alien.body.velocity.y = 40;
                        alien.checkWorldBounds = true;
                        alien.outOfBoundsKill = true;
                    }
                }
                // this.aliens.x = 150;
                // this.aliens.y = -100;
                break;
            }
            case 2: {
                game.add.tween(this.aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true).start();

                // this.aliens.x = 0;
                // this.aliens.y = 0;

                const genPromise = () => {
                    return new Promise((res, rej) => {
                        for (let x = 0; x < 10; x++) {
                            const alien = this.createEnemy(x * 48 + 100, 5);
                            alien.body.velocity.y = 40;
                            alien.checkWorldBounds = true;
                            alien.outOfBoundsKill = true;
                        }
                        setTimeout(() => { res(); }, 800);
                    });
                };
                for (let y = 0; y < 15; y++) {
                    await genPromise();
                }

                const hosPromise = () => {
                    return new Promise((res, rej) => {
                        const randX = getRandom(50, 500);
                        const randY = getRandom(0, 10);
                        const hospital = game.add.sprite(randX, randY, 'hospital', 0, this.hospitals);
                        hospital.body.velocity.y = getRandom(30, 50);
                        setTimeout(() => { res(); }, 500);
                    });
                };

                for (let x = 0; x < 10; x++) {
                    await hosPromise();
                }

                break;
            }
            case 3: {
                // this.aliens.x = 150;
                // this.aliens.y = 0;
                const genPromise = () => {
                    return new Promise((res, rej) => {
                        for (let x = 0; x < 10; x++) {
                            const alien = this.createEnemy(x * 48 + 150, 50);
                            alien.checkWorldBounds = true;
                            alien.outOfBoundsKill = true;

                            const random = game.rnd.integerInRange(0, 200);
                            const tween = game.add.tween(alien).to( { x: random }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    
                            tween.onRepeat.add(() => {
                                alien.y += game.rnd.integerInRange(50, 100)
                            }, this);

                            setTimeout(() => { res(); }, 600);
                        }
                    });
                };
                for (let y = 0; y < 20; y++) {
                    await genPromise();
                    if (y%3 === 2) {
                        const randX = getRandom(100, 500);
                        const randY = getRandom(0, 50);
                        const hospital = game.add.sprite(randX, randY, 'hospital', 0, this.hospitals);
                        hospital.body.velocity.y = getRandom(30, 50);
                    }
                }
                break;
            }
            case 4: {
                // this.aliens.x = 0;
                // this.aliens.y = 0;
                for (let x=0; x<3; x++) {
                    const caveman = this.createCaveman(x*80 + 300, 50);
                    caveman.body.velocity.y = 10;

                    for (let y=0; y<30; y++) {
                        setTimeout(() => {
                            if (caveman.visible) {
                                const alien = this.createEnemyFour(caveman.body.center.x, caveman.body.center.y);
                                alien.checkWorldBounds = true;
                                alien.outOfBoundsKill = true;
                                alien.body.velocity.y = 50;
                            }
                        }, y*600);
                    }
                }

                break;
            }
            case 5: {
                this.bigAlien = game.add.sprite(DISPLAY_WIDTH/2, 200, 'bigAlien');
                game.physics.arcade.enable(this.bigAlien);
                this.bigAlien.anchor.setTo(0.5, 0.5);
                this.bigAlien.liveNum = 2000;

                const genPromise = () => {
                    return new Promise((res, rej) => {
                        for (let x = 0; x < 3; x++) {
                            const alien = this.createEnemyFour(0, 0);
                            alien.body.velocity.y = 100;
                            alien.checkWorldBounds = true;
                            alien.outOfBoundsKill = true;

                            if (x === 1) alien.body.velocity.x = -60;
                            else if (x === 2) alien.body.velocity.x = 0;
                            else alien.body.velocity.x = 60;
                        }
                        setTimeout(() => { res() }, 300);
                    });
                }
                this.aliensFour.x = this.bigAlien.x;
                this.aliensFour.y = this.bigAlien.y;

                for (let y = 0; y < 200; y++) {
                    if (this.bigAlien.visible) await genPromise();
                }
                break;
            }
        }
    },
    gainLive: function(airplane, hospital) {
        const perIncrease = 15;
        if (liveNum + perIncrease >= 100) liveNum = 100;
        else liveNum += perIncrease;
        
        hospital.kill();
    },
    cavemanHitAirplane: function(airplane, caveman) {
        caveman.kill();

        this.hitAirplane(25);
    },
    cavemanHitBullet: function(caveman, bullet) {
        //  And create an explosion :)
        const explosion = this.explosions.getFirstExists(false);
        if (explosion) {
            explosion.reset(caveman.body.x, caveman.body.y);
            explosion.play('kaboom', 30, false, true);
        }
        const perDecrease = 30;
        if (caveman.lifeNum - perDecrease > 0) caveman.lifeNum -= perDecrease;
        else {
            caveman.lifeNum = 0;
            caveman.kill();
            score += 500;
        }
    },
    createCaveman: function(x, y) {
        const caveman = this.cavemans.create(x, y, 'caveman');
        caveman.anchor.setTo(0.5, 0.5);
        caveman.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        caveman.play('fly');
        caveman.body.moves = true;
        caveman.lifeNum = 1000;
        return caveman;
    },
    createEnemy: function(x, y) {
        const alien = this.aliens.create(x, y, 'invader');
        alien.anchor.setTo(0.5, 0.5);
        alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        alien.play('fly');
        alien.body.moves = true;
        return alien;
    },
    createEnemyFour: function(x, y) {
        const alien = this.aliensFour.create(x, y, 'invader');
        alien.anchor.setTo(0.5, 0.5);
        alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        alien.play('fly');
        alien.body.moves = true;
        return alien;
    },
    fireBullet: function () {
        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime && this.airplane.visible) {
            //  Grab the first bullet we can from the pool
            // let bullet = this.bullets.getFirstExists(false);
            const centerX = this.airplane.x;
            const centerY = this.airplane.y - 15;
            let bulletSet = [];
            bulletSet[0] = game.add.sprite(centerX - 20, centerY, 'bullet', 0, this.bullets);
            bulletSet[1] = game.add.sprite(centerX, centerY, 'bullet', 0, this.bullets);
            bulletSet[2] = game.add.sprite(centerX + 20, centerY, 'bullet', 0, this.bullets);
    
            for (let i=0;i<3;i++) {
                bulletSet[i].body.velocity.y = -800;
            }
            this.shootingSound.play();
            bulletTime = game.time.now + 200;
        }
    },
    collisionHandler: function(bullet, alien) {
        //  When a bullet hits an alien we kill them both
        bullet.kill();
        // alien.kill();
        alien.destroy();

        //  Increase the score
        score += 20;

        //  And create an explosion :)
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 15);

        if (this.aliens.countLiving() == 0) {
            score += 1000;
            this.scoreText.text = scoreString + score;

            this.enemyBullets.callAll('kill', this);
            // stateText.text = " You Won, \n Click to restart";
            // stateText.visible = true;

            //the "click to restart" handler
            // game.input.onTap.addOnce(restart, this);
        }
    },
    collisionHandlerFour: function(bullet, alien) {
        //  When a bullet hits an alien we kill them both
        bullet.kill();
        alien.destroy();

        //  Increase the score
        score += 20;

        //  And create an explosion :)
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 15);

        if (this.aliens.countLiving() == 0) {
            score += 1000;
            this.scoreText.text = scoreString + score;

            this.enemyBullets.callAll('kill', this);
        }
    },
    enemyBulletHitsPlayer: function(airplane, bullet) {
        bullet.kill();

        this.hitAirplane(10);

        // When the player dies
        // if (lives.countLiving() < 1) {
        //     alien.kill();
        //     enemyBullets.callAll('kill');

            // stateText.text=" GAME OVER \n Click to restart";
            // stateText.visible = true;

            //the "click to restart" handler
            // game.input.onTap.addOnce(restart,this);
        // }
    },
    enemyHitsPlayer: function(airplane, alien) {
        alien.kill();

        this.hitAirplane(20);
    },
    hitAirplane: function (costLiveNum) {
        //  And create an explosion
        const explosion = this.explosions.getFirstExists(false);
        if (explosion) {
            explosion.reset(this.airplane.body.center.x, this.airplane.body.center.y);
            explosion.play('kaboom', 30, false, true);
        }

        if (liveNum - costLiveNum >= 0) liveNum -= costLiveNum;
        else liveNum = 0;

        if (liveNum <= 0) {
            this.airplane.kill();
            this.enemyBullets.callAll('kill');
        }
    },
    enemyFires: function () {
        //  Grab the first bullet we can from the pool
        const enemyBullet = this.enemyBullets.getFirstExists(false);
        const livingEnemies = [];

        this.aliens.forEachAlive(function(alien){
            // put every living enemy in an array
            livingEnemies.push(alien);
        });

        if (enemyBullet && livingEnemies.length > 0) {
            const random = game.rnd.integerInRange(0, livingEnemies.length - 1);

            // randomly select one of them
            const shooter = livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            game.physics.arcade.moveToObject(enemyBullet, this.airplane, 120);
            firingTimer = game.time.now + 2000;
        }
    },
    bigAlienFires: function () {
        const enemyBullet = this.enemyBullets.getFirstExists(false);
        enemyBullet.reset(this.bigAlien.body.x, this.bigAlien.body.y);
        game.physics.arcade.moveToObject(enemyBullet, this.airplane, 120);
        bigAlienFiringTimer = game.time.now + 1000;
    },
    bulletHitsBigAlien: function(bigAlien, bullet) {
        bullet.kill();

        const explosion = this.explosions.getFirstExists(false);
        if (explosion) {
            explosion.reset(bigAlien.body.center.x, bigAlien.body.center.y);
            explosion.play('kaboom', 30, false, true);
        }

        const perDecrease = 10;
        if (bigAlien.liveNum - perDecrease >= 0) bigAlien.liveNum -= perDecrease;
        else bigAlien.liveNum = 0;

        this.bigAlienLivebar.scale.setTo(bigAlien.liveNum/2000, 1.0);
        if (bigAlien.liveNum <= 0) {
            score += 2000;
            bigAlien.kill();
            this.bigAlienLivebar.kill();
        }
    },
    gameOverState: function() {
        this.gameOver.visible = true;

        if (!isProcess) {
            isProcess = true;
            const database = firebase.database();
            database.ref('score').push(score);
        }

        if (this.enterButton.isDown) {
            game.state.start('menu');

            bulletTime = 0;
            score = 0;
            firingTimer = 0;
            bigAlienFiringTimer = 0;
            cdTimer = 0;
            liveNum = 100;
            skillNum = 0;
            skillEndTime = 0;
            isMakingEnemy = false;

            storedData = null;
            isProcess = false;


            ultimateNum = 0;
            ultimateNumCdTime = 0;

            phase = FIXED_PHASE;
            this.bgmSound.destroy();
        }
    }
};

game.state.add('main', mainState);
// game.state.start('main');