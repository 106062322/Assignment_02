# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 

1. Jucify mechanisms :
- Level
> 此遊戲共分有 5 Phase，難度由易到難，需依序破關進行。
- Skill
> 除了基礎射擊（ keyboard: spacebar ）外，仍有多方向射擊 ( keyboard: W )。
> 多方向射擊有射擊時間限制，可透過右手邊的 UltimateBar 確定剩餘可射擊時間。
> **注意** 每射擊完後，必須等候 3 秒的時間，UltimateBar 才會開始回復。

2. Animations :
> 敵人（蝙蝠）與 敵人（Caveman）有設定 animation 。
> 當被子彈射擊時，會有爆炸效果。

3. Particle Systems :
> 當敵人（蝙蝠）被子彈射擊時，會產生 Particle 粒子效果。

4. Sound effects :
> 共有 背景音樂 與 子彈射擊聲。
> 而音量大小的控制，可於畫面右方點擊喇叭圖示進行增減。

5. Leaderboard :
> 串連 Firebase Realtime Batabase，當每次遊戲結束時，會儲存該次分數。
> 可於 ScoreBoard 面板中查看前三名。

# Bonus Functions Description : 
1. Boss :
> 於 Phase 4 中，有 Caveman 小 boss 的設計。
> 於 Phase 5 中，有 大飛行船 boss 的設計。
2. Invisible SKill :
> 按下 ( Keyboard: E )，會進入隱藏模式。隱藏模式中，飛機並不會有任何扣血的機制。
> 此操作有恢復時間 (cd time) ，待 cd time 補足後，才可再做操作。
3. Little Helper :
> 在遊戲過程中，皆有漂浮的補命袋可以攝取。
